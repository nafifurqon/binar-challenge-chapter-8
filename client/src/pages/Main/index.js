import React, { useState } from "react";
import "./index.css";
import NavBar from "../../components/NavBar";
import { CreatePage, UpdatePage, SearchPage } from "..";

const navList = [{ label: "Create" }, { label: "Update" }, { label: "Search" }];

const Main = () => {
  const [activeNav, setActiveNav] = useState(0);

  const handleClick = (newActive) => {
    setActiveNav(newActive);
  };

  return (
    <div
      className="card"
      style={{ width: "90vw", height: "80vh", overflowY: "auto" }}
    >
      <NavBar>
        {navList.map((item, index) => (
          <span
            key={index}
            className={
              activeNav === index
                ? "btn nav-item nav-link active"
                : "btn nav-item nav-link"
            }
            onClick={() => handleClick(index)}
          >
            {item.label}
          </span>
        ))}
      </NavBar>
      <div className="container my-3">
        {activeNav === 0 && <CreatePage />}
        {activeNav === 1 && <UpdatePage />}
        {activeNav === 2 && <SearchPage />}
      </div>
    </div>
  );
};

export default Main;
