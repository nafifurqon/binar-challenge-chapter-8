import React, { useState, useEffect } from "react";
import { Input, Button, Table } from "../../components";

const tableHead = [
  "No",
  "Username",
  "Email",
  "Password",
  "Experience",
  "Level",
];

const initPlayers = [
  {
    id: 1,
    username: "muhammad",
    email: "muhammad@gmail.com",
    password: "123456",
    exp: 1000,
    lvl: 1,
  },
  {
    id: 2,
    username: "nafi",
    email: "nafi@gmail.com",
    password: "123456",
    exp: 2000,
    lvl: 2,
  },
  {
    id: 3,
    username: "furqon",
    email: "furqon@gmail.com",
    password: "123456",
    exp: 3000,
    lvl: 3,
  },
  {
    id: 4,
    username: "diani",
    email: "diani@gmail.com",
    password: "123456",
    exp: 4000,
    lvl: 4,
  },
];

const initValues = {
  username: "",
  email: "",
  exp: 0,
  lvl: 0,
};

const SearchPage = () => {
  const [players, setPlayers] = useState(initPlayers);
  const [values, setValues] = useState(initValues);

  const handleChange = (type, value) => {
    setValues((prev) => ({ ...prev, [type]: value }));
  };

  const handleSubmit = (e) => {
    let tempArr = [...initPlayers];
    let arrUsername = [];
    let arrEmail = [];
    let arrExp = [];
    let arrLvl = [];

    if (values.username !== "") {
      arrUsername = tempArr.filter((item) =>
        item.username.toLowerCase().includes(values.username)
      );
    }

    if (values.email !== "") {
      arrEmail = tempArr.filter((item) =>
        item.email.toLowerCase().includes(values.email)
      );
    }

    if (+values.exp > 0) {
      arrExp = tempArr.filter((item) =>
        item.exp.toString().includes(values.exp)
      );
    }

    if (+values.lvl > 0) {
      arrLvl = tempArr.filter((item) =>
        item.lvl.toString().includes(values.lvl)
      );
    }

    if (
      values.username !== "" ||
      values.email !== "" ||
      +values.exp > 0 ||
      +values.lvl > 0
    ) {
      tempArr = [...arrUsername, ...arrEmail, ...arrExp, ...arrLvl];
    } else {
      tempArr = [...initPlayers];
    }

    setPlayers(tempArr);
    setValues(initValues);

    e.preventDefault();
  };

  useEffect(() => (document.title = "Search Player"), []);

  return (
    <div className="row">
      <div className="col-md-6">
        <form autoComplete="off" onSubmit={(e) => handleSubmit(e)}>
          <Input
            name="username"
            label="Username"
            type="text"
            value={values.username}
            onChange={(e) => handleChange("username", e.target.value)}
          />
          <Input
            name="email"
            label="Email"
            type="text"
            value={values.email}
            onChange={(e) => handleChange("email", e.target.value)}
          />
          <Input
            name="exp"
            label="Experience"
            type="number"
            value={values.exp}
            onChange={(e) => handleChange("exp", e.target.value)}
          />
          <Input
            name="lvl"
            label="Level"
            type="number"
            value={values.lvl}
            onChange={(e) => handleChange("lvl", e.target.value)}
          />
          <div className="d-flex justify-content-end w-100">
            <Button label="Search" color="primary" />
          </div>
        </form>
      </div>
      <div className="col-md-6">
        <Table tableHead={tableHead} tableData={players} />
      </div>
    </div>
  );
};

export default SearchPage;
