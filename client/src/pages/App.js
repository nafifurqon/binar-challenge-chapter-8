import "./App.css";
import Main from "./Main";

function App() {
  return (
    <div
      className="d-flex justify-content-center align-items-center"
      style={{ height: "100vh" }}
    >
      <Main />
    </div>
  );
}

export default App;
