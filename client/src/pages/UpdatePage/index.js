import React, { useState, useEffect } from "react";
import { Input, Button, Table, Alert } from "../../components";

const tableHead = [
  "Action",
  "No",
  "Username",
  "Email",
  "Password",
  "Experience",
  "Level",
];

const initValues = {
  username: "",
  email: "",
  password: "",
  exp: 0,
  lvl: 0,
};

const UpdatePage = () => {
  const [values, setValues] = useState(initValues);
  const [disableForm, setDisableForm] = useState(true);
  const [showAlert, setShowAlert] = useState(false);

  const handleUpdate = (data) => {
    setDisableForm(false);
    setValues(data);
  };

  const initPlayers = [
    {
      action: (
        <Button
          onClick={() =>
            handleUpdate({
              id: 1,
              username: "muhammad",
              email: "muhammad@gmail.com",
              password: "123456",
              exp: 1000,
              lvl: 1,
            })
          }
          size="sm"
          color="info"
          label="view"
        />
      ),
      id: 1,
      username: "muhammad",
      email: "muhammad@gmail.com",
      password: "123456",
      exp: 1000,
      lvl: 1,
    },
    {
      action: (
        <Button
          onClick={() =>
            handleUpdate({
              id: 2,
              username: "nafi",
              email: "nafi@gmail.com",
              password: "123456",
              exp: 2000,
              lvl: 2,
            })
          }
          size="sm"
          color="info"
          label="view"
        />
      ),
      id: 2,
      username: "nafi",
      email: "nafi@gmail.com",
      password: "123456",
      exp: 2000,
      lvl: 2,
    },
    {
      action: (
        <Button
          onClick={() =>
            handleUpdate({
              id: 3,
              username: "furqon",
              email: "furqon@gmail.com",
              password: "123456",
              exp: 3000,
              lvl: 3,
            })
          }
          size="sm"
          color="info"
          label="view"
        />
      ),
      id: 3,
      username: "furqon",
      email: "furqon@gmail.com",
      password: "123456",
      exp: 3000,
      lvl: 3,
    },
    {
      action: (
        <Button
          onClick={(item) =>
            handleUpdate({
              id: 4,
              username: "diani",
              email: "diani@gmail.com",
              password: "123456",
              exp: 4000,
              lvl: 4,
            })
          }
          size="sm"
          color="info"
          label="view"
        />
      ),
      id: 4,
      username: "diani",
      email: "diani@gmail.com",
      password: "123456",
      exp: 4000,
      lvl: 4,
    },
  ];

  const [players, setPlayers] = useState(initPlayers);

  const handleChange = (type, value) => {
    setValues((prev) => ({ ...prev, [type]: value }));
  };

  const handleSubmit = (e) => {
    console.log(`values`, values);
    const tempArr = players.filter((item) => item.id !== values.id);
    console.log(`tempArr`, tempArr);
    tempArr.push({
      id: values.id,
      username: values.username,
      email: values.email,
      password: values.password,
      exp: values.exp,
      lvl: values.lvl,
      action: (
        <Button
          onClick={(item) =>
            handleUpdate({
              id: (players.length += 1),
              username: values.username,
              email: values.email,
              password: values.password,
              exp: values.exp,
              lvl: values.lvl,
            })
          }
          size="sm"
          color="info"
          label="view"
        />
      ),
    });
    setPlayers(tempArr);

    setValues(initValues);
    setDisableForm(true);
    setShowAlert(true);

    e.preventDefault();
  };

  useEffect(() => (document.title = "Update Player"), []);

  return (
    <>
      <Alert
        message="Successfully update player"
        visible={showAlert}
        onClose={() => setShowAlert(false)}
        type="success"
      />
      <div className="row">
        <div className="col-md-6">
          <form autoComplete="off" onSubmit={(e) => handleSubmit(e)}>
            <Input
              name="username"
              label="Username"
              type="text"
              value={values.username}
              onChange={(e) => handleChange("username", e.target.value)}
              required
              disabled={disableForm}
            />
            <Input
              name="email"
              label="Email"
              type="email"
              value={values.email}
              onChange={(e) => handleChange("email", e.target.value)}
              required
              disabled={disableForm}
            />
            <Input
              name="password"
              label="Password"
              type="password"
              value={values.password}
              onChange={(e) => handleChange("password", e.target.value)}
              required
              disabled={disableForm}
            />
            <Input
              name="exp"
              label="Experience"
              type="number"
              value={values.exp}
              onChange={(e) => handleChange("exp", e.target.value)}
              required
              disabled={disableForm}
            />
            <Input
              name="lvl"
              label="Level"
              type="number"
              value={values.lvl}
              onChange={(e) => handleChange("lvl", e.target.value)}
              required
              disabled={disableForm}
            />
            <div className="d-flex justify-content-end w-100">
              <Button disabled={disableForm} label="Update" color="primary" />
            </div>
          </form>
        </div>
        <div className="col-md-6">
          <Table
            tableHead={tableHead}
            tableData={players}
            // onClick={(item) => console.log(`item`, item)}
          />
        </div>
      </div>
    </>
  );
};

export default UpdatePage;
