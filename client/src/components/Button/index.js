import React from "react";

const Button = ({
  label = "Submit",
  color = "primary",
  size = "md",
  ...rest
}) => {
  return (
    <button className={`btn btn-${color} btn-${size}`} {...rest}>
      {label}
    </button>
  );
};

export default Button;
