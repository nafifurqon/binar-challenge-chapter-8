import React from "react";

const Input = ({ name, label, type, withLabel = true, ...rest }) => {
  return (
    <div className="form-group">
      {withLabel && <label htmlFor={name}>{label}</label>}
      <input
        className="form-control"
        id={name}
        placeholder={`${label}...`}
        name={name}
        type={type}
        {...rest}
      />
    </div>
  );
};

export default Input;
