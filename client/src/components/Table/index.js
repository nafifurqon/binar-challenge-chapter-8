import React from "react";

const initTableHead = ["No", "Column 1", "Column 2", "Column 3"];

const Table = ({ tableHead = initTableHead, tableData = [] }) => {
  return (
    <div className="table-responsive">
      <table className="table table-sm table-bordered table-hover">
        <thead>
          <tr>
            {tableHead.map((item, index) => (
              <th scope="col" key={index}>
                {item}
              </th>
            ))}
          </tr>
        </thead>
        <tbody>
          {tableData.length > 0 ? (
            tableData.map((item, index) => (
              <tr key={item.id}>
                {item?.action && <td>{item.action}</td>}
                <td>{(index += 1)}</td>
                <td>{item.username}</td>
                <td>{item.email}</td>
                <td>{item.password}</td>
                <td>{item.exp}</td>
                <td>{item.lvl}</td>
              </tr>
            ))
          ) : (
            <tr>
              <td colSpan={tableHead.length} className="text-center">
                Empty data
              </td>
            </tr>
          )}
        </tbody>
      </table>
    </div>
  );
};

export default Table;
