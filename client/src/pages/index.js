import CreatePage from "./CreatePage";
import UpdatePage from "./UpdatePage";
import SearchPage from "./SearchPage";

export { CreatePage, UpdatePage, SearchPage };
