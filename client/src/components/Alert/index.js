import React from "react";

const Alert = ({ message, visible = false, onClose, type = "warning" }) => {
  return (
    <div
      className={`alert alert-${type} alert-dismissible fade show ${
        visible ? `d-block` : `d-none`
      }`}
      role="alert"
    >
      {message}
      <button type="button" className="close" onClick={onClose}>
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
  );
};

export default Alert;
