import React, { useEffect, useState } from "react";
import { Input, Button, Table, Alert } from "../../components";

const tableHead = [
  "No",
  "Username",
  "Email",
  "Password",
  "Experience",
  "Level",
];

const initValues = {
  username: "",
  email: "",
  password: "",
  exp: 0,
  lvl: 0,
};

const CreatePage = () => {
  const [values, setValues] = useState(initValues);
  const [players, setPlayers] = useState([]);
  const [showAlert, setShowAlert] = useState(false);

  const handleChange = (type, value) => {
    setValues((prev) => ({ ...prev, [type]: value }));
  };

  const handleSubmit = (e) => {
    const tempArr = [...players];
    tempArr.push({
      id: (players.length += 1),
      username: values.username,
      email: values.email,
      password: values.password,
      exp: values.exp,
      lvl: values.exp > 0 ? Math.floor(values.exp / 1000) : 0,
    });
    setPlayers(tempArr);

    // set init form value
    setValues(initValues);
    setShowAlert(true);

    e.preventDefault();
  };

  useEffect(() => (document.title = "Create Player"), []);

  return (
    <>
      <Alert
        message="Successfully create player"
        visible={showAlert}
        onClose={() => setShowAlert(false)}
        type="success"
      />
      <div className="row">
        <div className="col-md-6">
          <form autoComplete="off" onSubmit={(e) => handleSubmit(e)}>
            <Input
              name="username"
              label="Username"
              type="text"
              value={values.username}
              onChange={(e) => handleChange("username", e.target.value)}
              required
            />
            <Input
              name="email"
              label="Email"
              type="email"
              value={values.email}
              onChange={(e) => handleChange("email", e.target.value)}
              required
            />
            <Input
              name="password"
              label="Password"
              type="password"
              value={values.password}
              onChange={(e) => handleChange("password", e.target.value)}
              required
            />
            <Input
              name="exp"
              label="Experience"
              type="number"
              value={values.exp}
              onChange={(e) => handleChange("exp", e.target.value)}
              // required
            />
            <div className="d-flex justify-content-end w-100">
              <Button label="Create" color="primary" />
            </div>
          </form>
        </div>
        <div className="col-md-6">
          <Table tableHead={tableHead} tableData={players} />
        </div>
      </div>
    </>
  );
};

export default CreatePage;
